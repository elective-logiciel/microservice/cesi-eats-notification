import { Router } from 'express';
import { notificationService } from '../datas/notification.data';
import { Notification } from '../models/notification.model';
import { returnSuccess, returnCreated, returnUpdated, returnDeleted } from '../errors/success';

const notificationController = Router();
const notificationSvc = new notificationService()

notificationController.get('/get/:id_notification',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        const notif_res = await notificationSvc.GetNotificationById(notif)

        const message: string = "Get notification by id"
        returnSuccess(res, notif_res, message)

    } catch (err) {
        next(err)
    }
})

notificationController.get('/user/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_user: req.params.id_user
        })

        notif.IsIdUserValid()

        const notif_res = await notificationSvc.GetAllNotificationsByIdUser(notif)

        const message: string = "Get notifications by id_user"
        returnSuccess(res, notif_res, message)

    } catch (err) {
        next(err)
    }
})

notificationController.post('/add/',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)


        const notif: Notification = new Notification({
            id_user: req.body.id_user,
            message: req.body.message,
            data: req.body.data,
        })

        notif.IsIdUserValid()
        notif.IsMessageValid()

        await notificationSvc.InsertNotification(notif)

        const message: string = "Add notification"
        returnCreated(res, message)

    } catch (err) {
        next(err)
    }
})

notificationController.put('/view/:id_notification', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        await notificationSvc.UpdateNotificationNew(notif)

        const message: string = "Update notification by id"
        returnUpdated(res, message)
    } catch (err) {
        next(err)
    }
})

notificationController.delete('/delete/:id_notification', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_notification: req.params.id_notification
        })

        notif.IsIdNotificationValid()

        await notificationSvc.DeleteNotificationById(notif)

        const message: string = "Delete notification by id"
        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

notificationController.delete('/user/delete/', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const notif: Notification = new Notification({
            id_user: req.body.id_user
        })

        notif.IsIdUserValid()

        await notificationSvc.DeleteAllNotificationsByIdUser(notif)

        const message: string = "Delete notifications by id_user"
        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

export { notificationController }