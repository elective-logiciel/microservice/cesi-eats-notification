import { connect, Schema, model } from 'mongoose';
import { Notification } from '../models/notification.model';

const config = require('../config')

const mongoDb = connect(config.mongo.server + config.mongo.database);

const NotificationSchema = new Schema<Notification>({
    id_user: { type: Number, required: true },
    message: { type: String, required: true },
    data: { type: {}, required: false},
    time: { type: Date, default: Date.now },
    new: { type: Boolean, default: true }
})

export const NotificationModel = model<Notification>('Notification', NotificationSchema)
