import { NotificationModel } from "../services/mongo.service";
import { Notification } from "../models/notification.model";

export class notificationService {
    public async GetNotificationById(notif: Notification) {
        const res = await NotificationModel.findById(notif.id_notification);

        return res
    }

    public async GetAllNotificationsByIdUser(notif: Notification) {
        const res = await NotificationModel.find({ "id_user": notif.id_user });

        return res
    }

    public async InsertNotification(notif: Notification) {
        var notification = new NotificationModel(notif)

        notification.save()
    }

    public async UpdateNotificationNew(notif: Notification) {
        const filter = { _id: notif.id_notification };
        const update = {
            new: false
        };

        await NotificationModel.findOneAndUpdate(filter, update); 
    }

    public async DeleteNotificationById(notif: Notification) {
        await NotificationModel.findByIdAndDelete(notif.id_notification)
    }

    public async DeleteAllNotificationsByIdUser(notif: Notification) {
        await NotificationModel.deleteMany({ "id_user": notif.id_user });
    }
}